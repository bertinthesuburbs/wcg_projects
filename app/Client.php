<?php

namespace App;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //
    use Searchable;

    protected $fillable = ['name','address', 'suburb', 'state', 'postcode', 'additional', 'legacy'];

    public function project(){
        return $this->hasMany(Project::class);
    }
}
