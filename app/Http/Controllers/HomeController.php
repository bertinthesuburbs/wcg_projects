<?php

namespace App\Http\Controllers;

use App\Client;
use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function search($terms) {
        $clients = Client::all()->toArray();
        $projects = DB::table('projects')
            ->select(DB::raw('*'))
            ->where('name', 'like', $terms)
            ->get();

        return view('search');
    }
}
