<?php

namespace App\Http\Controllers;

use App\Department;
use App\User;
use App\Client;
use Illuminate\Http\Request;
use App\Project;
use Illuminate\Support\Facades\DB;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $projects = Project::paginate(15);
        return view('projects.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $clients = Client::all()->toArray();
        $managers = User::all()->toArray();
        $departments = Department::all()->toArray();
        return view('projects.create', compact(['clients', 'managers', 'departments']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $project = $this->validate(request(), [
            'code' => 'required|unique:projects,code',
            'name' => 'required',
            'client_id' => 'required|numeric',
            'address' => 'required',
            'suburb' => 'required',
            'postcode' => 'required|size:4',
            'manager' => 'required|numeric',
            'department_id' => 'required|numeric',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after_or_equal:start_date'
        ]);

        Project::create($project);

        return back()->with('success', 'Project has been added');;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $project = Project::find($id);
        $clients = Client::all()->toArray();
        return view('projects.show',compact(['project','clients'],'id'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $project = Project::find($id);
        $clients = Client::all()->toArray();
        $managers = User::all()->toArray();
        $departments = Department::all()->toArray();
        return view('projects.edit',compact(['project','clients','managers','departments'],'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $project = Project::find($id);
        $this->validate(request(), [
            'code' => 'required',
            'name' => 'required',
            'client_id' => 'required|numeric',
            'address' => 'required',
            'suburb' => 'required',
            'postcode' => 'required|size:4',
            'manager' => 'required|numeric',
            'department_id' => 'required|numeric',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after_or_equal:start_date'
        ]);
        $project->code = $request->get('code');
        $project->name = $request->get('name');
        $project->client_id = $request->get('client_id');
        $project->address = $request->get('address');
        $project->suburb = $request->get('suburb');
        $project->postcode = $request->get('postcode');
        $project->manager = $request->get('manager');
        $project->department_id = $request->get('department_id');
        $project->start_date = $request->get('start_date');
        $project->end_date = $request->get('end_date');

        $client = Client::find($project->client_id);
        $project->client()->associate($client);

        $user = User::find($project->manager);
        $project->user()->associate($user);

        $department = Department::find($request->get('department_id'));
        $project->department()->associate($department);

        $project->save();
        return redirect('projects')->with('success','Project has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $project = Project::find($id);
        $project->delete();
        return redirect('projects')->with('success','Project has been  deleted');
    }

}
