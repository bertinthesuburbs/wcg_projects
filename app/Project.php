<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    //
    protected $fillable = [
        'code',
        'name',
        'client_id',
        'address',
        'suburb',
        'postcode',
        'manager',
        'department_id',
        'start_date',
        'end_date'
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'manager', 'id');
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }
}
