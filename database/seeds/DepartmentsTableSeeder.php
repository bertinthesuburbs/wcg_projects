<?php

use Illuminate\Database\Seeder;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert([
            'name' => 'drafting',
        ]);
        DB::table('departments')->insert([
            'name' => 'environment',
        ]);
        DB::table('departments')->insert([
            'name' => 'landscape',
        ]);
        DB::table('departments')->insert([
            'name' => 'planning',
        ]);
        DB::table('departments')->insert([
            'name' => 'surveying',
        ]);
        DB::table('departments')->insert([
            'name' => 'urban design',
        ]);
    }
}
