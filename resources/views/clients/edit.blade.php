<!-- edit.blade.php -->
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">Edit Client</div>
            <div class="panel-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                @endif
                <form method="post" action="{{action('ClientController@update', $id)}}">
                    {{csrf_field()}}
                    <input name="_method" type="hidden" value="PATCH">
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="form-group col-md-4">
                            <label for="name">Name:</label>
                            <input type="text" class="form-control" name="name" value="{{$client->name}}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="form-group col-md-4">
                            <label for="address">Address:</label>
                            <input type="text" class="form-control" name="address" value="{{$client->address}}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="form-group col-md-4">
                            <label for="suburb">Suburb:</label>
                            <input type="text" class="form-control" name="suburb" value="{{$client->suburb}}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="form-group col-md-4">
                            <label for="state">State:</label>
                            <input type="text" class="form-control" name="state" value="{{$client->state}}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="form-group col-md-4">
                            <label for="postcode">Postcode:</label>
                            <input type="text" class="form-control" name="postcode" value="{{$client->postcode}}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="form-group col-md-4">
                            <label for="additional">Additional Notes:</label>
                            <textarea id="additional" name="additional" class="form-control" value="{{$client->additional}}">
                                    </textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="form-group col-md-4">
                            <button type="submit" class="btn btn-success" style="margin-left:38px">Update Client</button>
                        </div>
                    </div>
            </form>
            </div>
        </div>
    </div>
@endsection
