<!-- index.blade.php -->
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">All Clients</div>
            <div class="panel-body">
    @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
    @endif
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Name</th>
            @guest
            <th colspan="2"></th>
            @else
            <th colspan="2">Action</th>
            @endguest
        </tr>
        </thead>
        <tbody>
        @foreach($clients as $client)
            <tr>
                <td><a href="{{action('ClientController@show', $client['id'])}}">{{$client['name']}}</a></td>
                @guest
                <td></td>
                <td></td>
                @else
                <td><a href="{{action('ClientController@edit', $client['id'])}}" class="btn btn-sm btn-warning">Edit</a></td>
                <td>
                    <form action="{{action('ClientController@destroy', $client['id'])}}" method="post">
                        {{csrf_field()}}
                        <input name="_method" type="hidden" value="DELETE">
                        <button class="btn btn-sm btn-danger" type="submit">Delete</button>
                    </form>
                </td>
                @endguest
            </tr>
        @endforeach
        </tbody>
    </table>
        {{ $clients->links() }}
    </div>
@endsection