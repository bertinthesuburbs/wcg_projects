@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <h3>{{$client->name}}</h3>
                        <address>
                            {{$client->address}}<br /> {{strtoupper($client->suburb)}}, {{$client->state}} {{$client->postcode}}
                        </address>
                        @guest

                        @else
                            <p>
                            <a href="{{action('ClientController@edit', $client['id'])}}" class="btn btn-sm btn-warning">Edit</a>
                            </p>
                        @endguest
                    </div>
                    <div class="col-md-6">

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p>{{$client->additional}}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h4>Projects</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Project</th>
                                <th>Address</th>
                                <th>Suburb</th>
                                <th>Postcode</th>
                            </tr>
                            </thead>
                            <tbody>
                    @foreach($projects as $project)
                        @if($project['client_id'] == $client['id'])
                            <tr>
                                <td><a href="{{action('ProjectController@edit', $project['id'])}}">{{$project['name']}}</a></td>
                                <td>{{$project['address']}}</td>
                                <td>{{$project['suburb']}}</td>
                                <td>{{$project['postcode']}}</td>
                            </tr>
                        @endif
                    @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection