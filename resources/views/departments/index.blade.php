<!-- index.blade.php -->
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">All Departments</div>
            <div class="panel-body">
    @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
    @endif
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Name</th>
            <th>Users</th>
            @guest
            @else
            <th colspan="2">Action</th>
            @endguest
        </tr>
        </thead>
        <tbody>
        @foreach($departments as $department)
            <tr>
                <td>{{ucwords($department['name'])}}</td>
                <td>
                    @foreach($users as $user)
                        @if($user['department_id'] == $department['id'])
                            {{$user['name']}},
                        @endif
                    @endforeach
                </td>
                @guest
                @else
                <td><a href="{{action('DepartmentController@edit', $department['id'])}}" class="btn btn-sm btn-warning">Edit</a></td>
                <td>
                    <form action="{{action('DepartmentController@destroy', $department['id'])}}" method="post">
                        {{csrf_field()}}
                        <input name="_method" type="hidden" value="DELETE">
                        <button class="btn btn-sm btn-danger" type="submit">Delete</button>
                    </form>
                </td>
                @endguest
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endsection