<!-- create.blade.php -->

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Laravel 5.5 CRUD Tutorial With Example From Scratch </title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>
<body>
<div class="container">
    <h2>Create A Project</h2><br  />
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
    @endif
    @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
    @endif
    <form method="post" action="{{url('projects')}}">
        {{csrf_field()}}
        <div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="code">Code:</label>
                <input id="code" type="text" class="form-control" name="code">
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="name">Name:</label>
                <input id="name" type="text" class="form-control" name="name">
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="client_id">Client:</label>
                <select id="client_id" class="form-control" name="client_id">
                    <option value="null">Choose a client.</option>
                    @foreach($clients as $client)
                        <option value="{{$client['id']}}">{{ucwords($client['name'])}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="address">Address:</label>
                <input id="address" type="text" class="form-control" name="address">
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="suburb">Suburb:</label>
                <input id="suburb" type="text" class="form-control" name="suburb">
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="postcode">Postcode:</label>
                <input id="postcode" type="text" class="form-control" name="postcode">
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="manager">Manager:</label>
                <select id="manager" class="form-control" name="manager">
                    <option value="null">Choose a project manager.</option>
                    @foreach($managers as $manager)
                        <option value="{{$manager['id']}}">{{ucfirst($manager['name'])}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="department_id">Department:</label>
                <select id="department_id" class="form-control" name="department_id">
                    <option value="null">Choose a project department.</option>
                    @foreach($departments as $department)
                        <option value="{{$department['id']}}">{{ucfirst($department['name'])}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="start_date">Start Date:</label>
                <input id="start_date" type="date" class="form-control" name="start_date">
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="end_date">End Date:</label>
                <input id="end_date" type="date" class="form-control" name="end_date">
            </div>
        </div>
    </div>
    <div class="row">
    <div class="col-md-4"></div>
    <div class="form-group col-md-4">
        <button type="submit" class="btn btn-success" style="margin-left:38px">Add Project</button>
    </div>
</div>
</form>
</div>
</body>
</html>