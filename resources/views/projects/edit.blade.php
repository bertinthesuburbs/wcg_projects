<!-- edit.blade.php -->

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Laravel 5.5 CRUD Tutorial With Example From Scratch </title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>
<body>
<div class="container">
    <h2>Edit A Project</h2><br  />
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
    @endif
    <form method="post" action="{{action('ProjectController@update', $id)}}">
        {{csrf_field()}}
        <input name="_method" type="hidden" value="PATCH">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="code">Code:</label>
                <input id="code" type="text" class="form-control" name="code" value="{{$project->code}}">
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="name">Name:</label>
                <input id="name" type="text" class="form-control" name="name" value="{{$project->name}}">
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="client_id">Client:</label>
                <select id="client_id" class="form-control" name="client_id" autocomplete="off">
                    @foreach($clients as $client)
                        @if($project->client_id == $client['id'] )
                            <option value="{{$client['id']}}" selected >{{ucfirst($client['name'])}}</option>
                        @else
                            <option value="{{$client['id']}}">{{ucfirst($client['name'])}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="address">Address:</label>
                <input id="address" type="text" class="form-control" name="address" value="{{$project->address}}">
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="suburb">Suburb:</label>
                <input id="suburb" type="text" class="form-control" name="suburb" value="{{$project->suburb}}">
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="postcode">Postcode:</label>
                <input id="postcode" type="text" class="form-control" name="postcode" value="{{$project->postcode}}">
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="manager">Manager:</label>
                <select id="manager" class="form-control" name="manager" autocomplete="off">
                    @foreach($managers as $manager)
                        @if($project->manager == $manager['id'] )
                            <option value="{{$manager['id']}}" selected >{{ucfirst($manager['name'])}}</option>
                        @else
                            <option value="{{$manager['id']}}">{{ucfirst($manager['name'])}}</option>
                        @endif
                    @endforeach
                </select>
                
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="department_id">Department:</label>
                <select id="department_id" class="form-control" name="department_id" autocomplete="off">
                    @foreach($departments as $department)
                        @if($project->department_id == $department['id'] )
                            <option value="{{$department['id']}}" selected >{{ucfirst($department['name'])}}</option>
                        @else
                            <option value="{{$department['id']}}">{{ucwords($department['name'])}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="start_date">Start Date:</label>
                <input id="start_date" type="date" class="form-control" name="start_date" value="{{$project->start_date}}">
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="end_date">End Date:</label>
                <input id="end_date" type="date" class="form-control" name="end_date" value="{{$project->end_date}}">
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <button type="submit" class="btn btn-success">Update Project</button>
            </div>
        </div>
</form>
</div>
</body>
</html>