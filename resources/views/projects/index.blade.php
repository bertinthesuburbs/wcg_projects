<!-- index.blade.php -->
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            Search Projects
        </div>
        <div class="panel-body">
            <form action="/search" method="POST" role="search">
                {{ csrf_field() }}
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-2"><label for="searchname">Project Name:</label></div>
                        <div class="col-md-8"><input type="text" class="form-control" id="searchname" placeholder="TO IMPLEMENT"></div>
                        <div class="col-md-2"><button type="submit" class="btn btn-default">Search Projects</button></div>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <div class="panel panel-default">
        <div class="panel-heading">All Projects</div>
        <div class="panel-body">
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div><br />
        @endif
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Code</th>
                <th>Name</th>
                <th>Client</th>
                <th>Address</th>
                <th>Suburb</th>
                <th>Postcode</th>
                <th>Manager</th>
                <th>Department</th>
                @guest
                @else
                <th colspan="2">Action</th>
                @endguest
            </tr>
            </thead>
            <tbody>
            @foreach($projects as $project)
                <tr>
                    <td>{{$project['code']}}</td>
                    <td><a href="{{action('ProjectController@show', $project['id'])}}">{{$project['name']}}</a></td>
                    <td><a href="{{action('ClientController@show', $project['client_id'])}}">{{$project->client->name}}</a></td>
                    <td>{{$project['address']}}</td>
                    <td>{{strtoupper($project['suburb'])}}</td>
                    <td>{{$project['postcode']}}</td>
                    <td>{{$project->user->name}}</td>
                    <td>{{ucwords($project->department->name)}}</td>
                    @guest
                    @else
                    <td><a href="{{action('ProjectController@edit', $project['id'])}}" class="btn btn-sm btn-warning">Edit</a></td>
                    <td>
                        <form action="{{action('ProjectController@destroy', $project['id'])}}" method="post">
                            {{csrf_field()}}
                            <input name="_method" type="hidden" value="DELETE">
                            <button class="btn btn-sm btn-danger" type="submit">Delete</button>
                        </form>
                    </td>
                    @endguest
                </tr>
            @endforeach
            </tbody>
        </table>
            {{ $projects->links() }}
    </div>
    </div>
</div>
@endsection