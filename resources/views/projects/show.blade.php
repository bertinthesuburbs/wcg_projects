@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4"><h3>{{$project->name}}</h3></div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h4>Project Address:</h4>
                <p>{{$project->address}}<br /> {{strtoupper($project->suburb)}}, {{$project->state}} {{$project->postcode}}</p>
            </div>
            <div class="col-md-4">
                <h4>Project Manager:</h4>
                <p>{{$project->user->name}}</p>
            </div>
            <div class="col-md-4">
                <h4>Project Department:</h4>
                <p>{{$project->department->name}}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h4>Project Location:</h4>
            </div>
            <div class="col-md-4">
                <h4>Project Start Date:</h4>
                <p>{{$project->start_date}}</p>
            </div>
            <div class="col-md-4">
                <h4>Project End Date:</h4>
                <p>{{$project->end_date}}</p>
            </div>
        </div>
    </div>

@endsection