<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Project;
use Illuminate\Support\Facades\Input;

Route::get('/', function () {
    return view('home');
});

Route::resource('departments','DepartmentController');
Route::resource('clients','ClientController');
Route::resource('projects','ProjectController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');